# todo.txt gnome-shell extension translation file
# This file is distributed under the same license as the todo.txt gnome-shell extension.
# Translators:
# Nuno Martins <nc.martins@campus.fct.unl.pt>, 2015-2016
msgid ""
msgstr ""
"Project-Id-Version: todo.txt gnome-shell extension\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-04-25 15:03+0200\n"
"PO-Revision-Date: 2016-08-31 09:23+0200\n"
"Last-Translator: Nuno Martins <nc.martins@campus.fct.unl.pt>\n"
"Language-Team: Portuguese (Portugal) (http://www.transifex.com/bart-libert/todotxt-gnome-shell-extension/language/pt_PT/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_PT\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../errors.js:34
msgid "%(file) cannot be written. Please check its permissions"
msgstr "%(file) não pode ser escrito. Por favor verifique as suas permissões"

#: ../errors.js:45
msgid "An error occured while writing to %(file): %(error)"
msgstr "Um erro ocorreu ao escrever no ficheiro %(file): %(error)"

#: ../extension.js:135
msgid "New task..."
msgstr "Nova tarefa..."

#: ../extension.js:312
msgid "Open todo.txt file in text editor"
msgstr "Abrir ficheiro todo.txt no editor de texto"

#: ../extension.js:319
msgid "Cannot open file"
msgstr "Não é possível abrir o ficheiro"

#: ../extension.js:321
msgid ""
"An error occured while trying to launch the default text editor: %(error)"
msgstr "Um erro ocorreu ao tentar lançar o editor de texto pré-definido: %(error)"

#: ../extension.js:403
msgid "Ungrouped"
msgstr "Desagrupadas"

#: ../extension.js:524
msgid "Use existing file"
msgstr "Utilizar ficheiro existente"

#: ../extension.js:529
msgid "Create new file"
msgstr "Criar novo ficheiro"

#: ../extension.js:535
msgid "Open settings"
msgstr "Abrir definições"

#: ../extension.js:539
msgid "Cancel"
msgstr "Cancelar"

#: ../extension.js:542
msgid "%(file) exists already"
msgstr "%(file) já existente"

#: ../extension.js:543
msgid "Please choose what you want to do"
msgstr "Por favor escolha o que deseja fazer"

#: ../extension.js:561
msgid "No valid %(filename) file specified"
msgstr "Não foi especificado um ficheiro %(filename) válido"

#: ../extension.js:562
msgid "Select location in settings"
msgstr "Selecionar localização nas definições"

#: ../extension.js:567
msgid "Create todo.txt and done.txt file in %(path)"
msgstr "Criar ficheiros todo.txt e done.txt em %(path)"

#: ../extension.js:812 ../extension.js:870
msgid "Error writing file"
msgstr "Erro ao escrever ficheiro"

#: ../extension.js:823 ../extension.js:877
msgid "Unknown error during file write: %(error)"
msgstr "Erro desconhecido ao escrever ficheiro: %(error)"

#: ../messageDialog.js:40
msgid "Ok"
msgstr "Confirmar"

#: ../todoMenuItem.js:175
msgid "Edit %(task)"
msgstr "Editar %(task)"

#: ../todoMenuItem.js:185
msgid "Increase %(task) priority"
msgstr "Incrementar a prioridade de %(task)"

#: ../todoMenuItem.js:186
msgid "Decrease %(task) priority"
msgstr "Decrementar a prioridade de %(task)"

#: ../todoMenuItem.js:201
msgid "Archive %(task)"
msgstr "Arquivar %(task)"

#: ../todoMenuItem.js:211 ../todoMenuItem.js:456
msgid "Delete %(task)"
msgstr "Eliminar %(task)"

#: ../todoMenuItem.js:232
msgid "Expand %(task)"
msgstr "Expandir %(task)"

#: ../todoMenuItem.js:246
msgid "Mark %(task) as done"
msgstr "Marcar %(task) como concluída"

#: ../todoMenuItem.js:478
msgid "Undo delete %(task)"
msgstr "Desfazer eliminar %(task)"

#: ../todoMenuItem.js:488
msgid "Are you sure?"
msgstr "Tem a certeza?"

#: ../preferences/pathWidget.js:16
msgid "Select file"
msgstr "Selecionar ficheiro"

#: ../preferences/pathWidget.js:49
msgid "Browse"
msgstr "Navegar"

#: ../preferences/preferenceWidget.js:124
msgid "No description"
msgstr "Sem descrição"

#: ../preferences/priorityMarkupWidget.js:93
msgid "Wrong priority"
msgstr "Prioridade inválida"

#: ../preferences/priorityMarkupWidget.js:107
msgid "Duplicate priority: %(priority)"
msgstr "Prioridade duplicada: %(priority)"

#: ../preferences/priorityMarkupWidget.js:206
msgid "Priority"
msgstr "Prioridade"

#: ../preferences/priorityMarkupWidget.js:230
msgid "Change color"
msgstr "Alterar cor"

#: ../preferences/priorityMarkupWidget.js:233
msgid "Color"
msgstr "Cor"

#: ../preferences/priorityMarkupWidget.js:267
msgid "Bold"
msgstr "Negrito"

#: ../preferences/priorityMarkupWidget.js:268
msgid "Italic"
msgstr "Itálico"

#: ../preferences/priorityMarkupWidget.js:279
msgid "Add style"
msgstr "Adicionar estilo"

#: ../preferences/priorityMarkupWidget.js:286
msgid "Please enter the priority"
msgstr "Por favor introduza a prioridade"

#: ../preferences/priorityMarkupWidget.js:288
msgid "New priority style"
msgstr "Novo estilo de prioridade"

#: ../preferences/priorityMarkupWidget.js:309
msgid "Delete"
msgstr "Eliminar"

#: ../preferences/shortcutWidget.js:36
msgid "Function"
msgstr "Função"

#: ../preferences/shortcutWidget.js:78
msgid "Key"
msgstr "Atalho"

#: ../preferences/subCategoryTab.js:50
msgid "Help"
msgstr "Ajuda"

# settings.json
msgid "Location of the text file that contains completed (archived) tasks"
msgstr "Localização do ficheiro de texto que contém as tarefas completas (arquivadas)"

# settings.json
msgid "Auto-add creation date to new tasks"
msgstr "Auto-adicionar data de criação a novas tarefas"

# settings.json
msgid "Expert"
msgstr "Perito"

# settings.json
msgid "Whether a button is shown to edit a task"
msgstr "Se é apresentado um botão que permite a edição de tarefas"

# settings.json
msgid "Open task list"
msgstr "Abrir lista de tarefas"

# settings.json
msgid "Top Bar"
msgstr "Barra de topo"

# settings.json
msgid "Files"
msgstr "Ficheiros"

# settings.json
msgid "Hide if pattern is zero"
msgstr "Esconder se o padrão for zero"

# settings.json
msgid "Scroll up/down to contract/expand"
msgstr "Rolar para cima/baixo para contrair/expandir"

# settings.json
msgid "Icon"
msgstr "Ícone"

# settings.json
msgid "Put ungrouped tasks in separate group"
msgstr "Colocar tarefas desagrupadas em grupo separado"

# settings.json
msgid "Both"
msgstr "Ambos"

# settings.json
msgid "Mark task as done or archive task"
msgstr "Marcar tarefa como concluída ou arquivar tarefa"

# settings.json
msgid "Whether projects are shown in the interface (does not affect grouping)"
msgstr "Se os projetos são apresentados na interface (agrupamento não afetado)"

# settings.json
msgid "Extensions"
msgstr ""

# settings.json
msgid "Debug"
msgstr "Debug"

# settings.json
msgid "Display"
msgstr "Visualização"

# settings.json
msgid "Show projects"
msgstr "Mostrar projetos"

# settings.json
msgid "Level of settings that is shown"
msgstr "Nível das definições a mostrar"

# settings.json
msgid "Pattern to match for zero"
msgstr "Padrão a corresponder para zero"

# settings.json
msgid "Select location of done.txt file"
msgstr "Selecionar localização do ficheiro done.txt"

# settings.json
msgid "Truncate long tasks"
msgstr "Truncar tarefas longas"

# settings.json
msgid "This color will be used for URLs if 'Custom color' was selected above"
msgstr "Esta cor será utilizada para os URLs se 'Cor personalizada' estiver selecionada acima"

# settings.json
msgid "Warning"
msgstr "Aviso"

# settings.json
msgid "Advanced"
msgstr "Avançado"

# settings.json
msgid "Same as task"
msgstr "Igual à tarefa"

# settings.json
msgid "Interface elements"
msgstr "Elementos da interface"

# settings.json
msgid "Show change task priority buttons"
msgstr "Mostrar botões para alterar prioridade"

# settings.json
msgid "What to do when a task is clicked"
msgstr "O que fazer quando uma tarefa é clicada"

# settings.json
msgid "Info"
msgstr "Info"

# settings.json
msgid "Settings level"
msgstr "Nível das definições"

# settings.json
msgid "End"
msgstr "Fim"

# settings.json
msgid "Style priorities"
msgstr "Estilizar prioridades"

# settings.json
msgid "Whether arrows are shown to increase or decrease the task priority"
msgstr "Se setas que permitem incrementar ou decrementar a prioridade de uma tarefa são apresentadas"

# settings.json
msgid "The way that tasks with different priorities are displayed"
msgstr "A forma como tarefas com prioridades diferentes são apresentadas"

# settings.json
msgid ""
"If the hidden extension is enabled, tasks containing 'h:1' will not be shown"
msgstr ""

# settings.json
msgid "Location of the text file that contains the tasks in todo.txt syntax"
msgstr "Localização do ficheiro de texto que contém as tarefas em sintaxe todo.txt"

# settings.json
msgid "Whether long tasks are truncated if they exceed a specified width"
msgstr "Se tarefas longas são truncadas ao excederem uma largura especificada"

# settings.json
msgid "Styles"
msgstr "Estilos"

# settings.json
msgid "Whether contexts are shown in the interface (does not affect grouping)"
msgstr "Se os contextos são apresentados na interface (agrupamento não afetado)"

# settings.json
msgid "Template string for display"
msgstr "String de 'template' para visualização"

# settings.json
msgid "Nothing"
msgstr "Nada"

# settings.json
msgid "Action on clicking task"
msgstr "Ação ao clicar tarefa"

# settings.json
msgid "Projects"
msgstr "Projetos"

# settings.json
msgid ""
"Tasks will be truncated to this width (specified in pixels) if truncating is"
" enabled"
msgstr "Tarefas serão truncadas a esta largura (em píxeis) se a truncagem estiver ativa"

# settings.json
msgid "Select location of todo.txt file"
msgstr "Selecionar localização do ficheiro todo.txt"

# settings.json
msgid "Priority on task completion"
msgstr "Prioridade ao completar tarefa"

# settings.json
msgid ""
"Whether a menu element is shown to open the todo.txt file in the default "
"text editor"
msgstr "Se um elemento de menu é apresentado para abrir o ficheiro todo.txt no editor de texto pré-definido"

# settings.json
msgid "Method to expand/contract truncated tasks"
msgstr "Método para expandir/contrair tarefas truncadas"

# settings.json
msgid "Grouping"
msgstr "Agrupamento"

# settings.json
msgid "Debug level"
msgstr "Nível de debug"

# settings.json
msgid "Custom color for URLS"
msgstr "Cor personalizada para URLs"

# settings.json
msgid "Truncating"
msgstr "Truncagem"

# settings.json
msgid "Show done/archive task button"
msgstr "Mostrar botão para completar/arquivar tarefa"

# settings.json
msgid ""
"\n"
"\n"
"Note that padding and | cannot be used for the zero-matching pattern"
msgstr ""

# settings.json
msgid "Whether tasks with a certain priority are shown in a specific style"
msgstr "Se tarefas com uma certa prioridade são apresentadas com um estilo especificado"

# settings.json
msgid "Detail"
msgstr "Detalhe"

# settings.json
msgid "Start"
msgstr "Início"

# settings.json
msgid "Whether a creation date is automatically added to newly created tasks"
msgstr "Se a data de criação é automaticamente adicionada a tarefas acabadas de criar"

# settings.json
msgid "Whether a confirmation dialog is shown before deleting a task"
msgstr "Se uma mensagem de confirmação é apresentada antes de eliminar uma tarefa"

# settings.json
msgid "Basic"
msgstr "Básico"

# settings.json
msgid "Priority markup"
msgstr "Marcação da prioridade"

# settings.json
msgid "Confirm task deletion"
msgstr "Confirmar eliminação da tarefa"

# settings.json
msgid ""
"Whether a button is shown to mark active tasks as completed or to archive "
"completed tasks, if auto-archive is off"
msgstr "Se um botão é apresentado para marcar tarefas ativas como completadas ou para arquivar tarefas completadas, se o arquivamento automático estiver desativado"

# settings.json
msgid "Keep with pri: prefix"
msgstr "Manter com pri: prefixo"

# settings.json
msgid "Keep as is (non-standard)"
msgstr "Manter como está (não é o padrão)"

# settings.json
msgid "Custom color"
msgstr "Cor personalizada"

# settings.json
msgid "When URLs are detected in a task, they will be displayed in this color"
msgstr "Quando URLs são detetados numa tarefa, eles serão apresentados nesta cor"

# settings.json
msgid "Color for detected URLs"
msgstr "Cor dos URLs detetados"

# settings.json
msgid "If this is true, an icon will be shown in the top bar"
msgstr "Se ativo, um ícone será apresentado na barra de topo"

# settings.json
msgid "Hidden tasks extension"
msgstr ""

# settings.json
msgid "Todo.txt location"
msgstr "Localização do todo.txt"

# settings.json
msgid ""
"Templates can contain the following patterns: \n"
"\t{undone}: number of tasks that are not completed yet\n"
"\t{unarchived}: number of tasks that are not archived yet\n"
"\n"
"If you surround an expression with pipe characters (|), the pattern will be mathematically evaluated after the replacements have been done.\n"
"For example: '{unarchived}-{undone}' will render as '3-2' for 3 unarchived and 2 undone tasks, but '|{unarchived}-{undone}| will render as '1'\n"
"\n"
"\n"
"You can also use a prefix to pad a number.\n"
"The prefix consists of three elements:\n"
"\tA number indicating the desired width. If the number is wider, no padding will be done\n"
"\tA letter indicating the padding direction:\n"
"\t\tl: Pad left\n"
"\t\tr: Pad right\n"
"\t\tL: Pad at both sides, but more at left side if uneven padding\n"
"\t\tR: Pad at both sides, but more at right side if uneven padding\n"
"\tThe character to pad with (optional, default is a space)\n"
"\tA ':' to split the prefix and the wildcard\n"
"\n"
"Examples:\n"
"\t{3lx:undone} will render as xx2 for 2 undone tasks\n"
"\t{4R0:unarchived} will render as 0300 if there are 3 unarchived tasks"
msgstr ""

# settings.json
msgid ""
"Tasks that don't have the grouping priority can be put in a special "
"'ungrouped' group, or shown outside any groups"
msgstr "Tarefas que não possuem grupo podem ser colocadas num grupo especial 'desagrupadas', ou apresentadas fora de todos os grupos"

# settings.json
msgid "Show number of tasks in group"
msgstr "Mostrar número de tarefas por grupo"

# settings.json
msgid "No grouping"
msgstr "Não agrupar"

# settings.json
msgid "Flow"
msgstr "Fluxo"

# settings.json
msgid "Template that determines what is displayed in the top bar"
msgstr "'Template' que determina o que é apresentado na barra de topo"

# settings.json
msgid "Priorities"
msgstr "Prioridades"

# settings.json
msgid "Auto-archive done tasks"
msgstr "Auto-arquivar tarefas completadas"

# settings.json
msgid "The number of tasks in a subgroup can be shown in the interface"
msgstr "O número de tarefas num sub-grupo pode ser mostrado na interface"

# settings.json
msgid "Error"
msgstr "Erro"

# settings.json
msgid "Maximum task width in pixels"
msgstr "Largura máxima da tarefa em píxeis"

# settings.json
msgid "Edit task"
msgstr "Editar tarefa"

# settings.json
msgid "Shortcuts"
msgstr "Atalhos"

# settings.json
msgid "Text"
msgstr "Texto"

# settings.json
msgid "General"
msgstr "Geral"

# settings.json
msgid "Location to truncate long tasks"
msgstr "Localização do truncar de tarefas longas"

# settings.json
msgid "Show icon"
msgstr "Mostrar ícone"

# settings.json
msgid "Show delete task button"
msgstr "Mostrar botão para eliminar tarefa"

# settings.json
msgid "Show new task entry"
msgstr "Mostrar campo de nova tarefa"

# settings.json
msgid "Level of debug information"
msgstr "Nível da informação de debug"

# settings.json
msgid "The location in the task text where the ellipsization will occur"
msgstr "A localização no texto da tarefa onde as reticências irão aparecer"

# settings.json
msgid "Show 'open in text editor'"
msgstr "Mostrar 'abrir no editor de texto'"

# settings.json
msgid "Done.txt location"
msgstr "Localização do done.txt"

# settings.json
msgid ""
"Whether an entry field is shown to create new tasks (new tasks can also be "
"added by modifying the todo.txt file)"
msgstr "Se um campo de texto é apresentado na interface para permitir a criação de novas tarefas (novas tarefas também podem ser adicionadas modificando o ficheiro todo.txt)"

# settings.json
msgid "Tasks can be grouped together based on the selected property"
msgstr "Tarefas podem ser agrupadas baseadas na opção selecionada"

# settings.json
msgid ""
"Whether completed tasks will be automatically archived (i.e. moved to the "
"done.txt file)"
msgstr "Se tarefas completadas são arquivadas automaticamente (ou seja, movidas para o ficheiro done.txt)"

# settings.json
msgid ""
"\n"
"\n"
"When using the 'hidden' extension, an extra pattern is available:\n"
"\t{hidden}: number of hidden tasks"
msgstr ""

# settings.json
msgid "Whether a button is shown to delete a task"
msgstr "Se um botão é apresentado para eliminar uma tarefa"

# settings.json
msgid ""
"What should be done with the priority of a task when that task is completed"
msgstr "O que deve ser efetuado à prioridade de uma tarefa quando essa tarefa é concluída"

# settings.json
msgid "Show edit task button"
msgstr "Mostrar botão para editar tarefa"

# settings.json
msgid "Group tasks by"
msgstr "Agrupar tarefas por"

# settings.json
msgid "Contexts"
msgstr "Contextos"

# settings.json
msgid ""
"If the specified pattern is zero, the elements specified here will be hidden"
msgstr "Se o padrão especificado for zero, os elementos aqui especificados serão ocultados"

# settings.json
msgid "Show contexts"
msgstr "Mostrar contextos"

# settings.json
msgid "Remove"
msgstr "Eliminar"

# settings.json
msgid "Dedicated button"
msgstr "Botão dedicado"

# settings.json
msgid "URL Color"
msgstr "Cor do URL"

# settings.json
msgid "Middle"
msgstr "Meio"

# settings.json
msgid "The action that will initiate expansion and contraction of tasks"
msgstr "A ação que irá iniciar a expansão e contração das tarefas"

# settings.json
msgid ""
"If this template evaluates to zero, the top bar element will be hidden. "
"Shortcuts still work."
msgstr "Se este 'template' avaliar para zero, o elemento na barra de topo será ocultado. Os atalhos de teclado continuarão a funcionar."

# settings.json
msgid "Get color from theme"
msgstr "Obter cor através do tema"
